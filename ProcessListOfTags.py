import sys
import json

tagsFile = sys.argv[1] #File with the JSON
listOfTags = "" #List to Dump back to the file
searchTerm = "" #If applicable, the string must start with this
data = json.load(open(tagsFile)) #

if len(sys.argv) >= 3: #If the search term was given
    searchTerm= sys.argv[2]+"-" #Forces the TAG to split with '-'

for tag in data:
    tagName = tag["name"]+"\n"
    if tagName.startswith(searchTerm): #If the searchTerm if "" this is always true
        listOfTags+=tagName

open(tagsFile,"w+").write(listOfTags) #Save into the file again
