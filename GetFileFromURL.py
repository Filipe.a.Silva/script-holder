import urllib.parse
import subprocess
import sys

#REMOVE GITLAB DOPMAIN IF IT EXISTS IN THE URL (NOT NEEDED)
URL = sys.argv[1]
if "gitlab.com/" in URL:
    URL = URL.split("gitlab.com/")[-1]

#ISOLATES THE PATH TO THE REPOSITORY FROM THE FILEPATH AND BRANCH
repositorypath,filepath = URL.split("/-/blob/")

#SAVES INTO TEMPORARY FILES FOR PRE-PROCESSING 
open(sys.argv[2],"w+").write(repositorypath)
open(sys.argv[3],"w+").write("/".join(filepath.split("/")[1:]))
open(sys.argv[4],"w+").write(filepath.split("/")[0])
