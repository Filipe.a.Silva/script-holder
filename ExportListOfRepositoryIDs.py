import sys
import re

#GET FILE CONTENT AND START A CLEAN STRING TO SAVE AFTER
fileToRead = sys.argv[1]
repositoryIds=""
regex ="^\s*project-id: ([0-9]+)\s*"

#LOOK FOR EVERY LINE WITH 'project-id' AND ISOLATES THE ID
for line in open(fileToRead,"r").readlines():
    if re.search(regex,line):
        repositoryIds += re.sub(regex, r"\1\n", line)
#SAVES THE IDS BACK TO THE FILE
open(fileToRead,"w+").write(repositoryIds)
