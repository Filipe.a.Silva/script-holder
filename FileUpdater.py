import re
import json
import urllib.parse,sys

changes = json.load(open("Changes.JSON")) #LOAD CHANGES FILES
filecontent = open(sys.argv[1],"r").read()
filename=sys.argv[2].split("/")[-1] #GET CURRENT FILE NAME

#MAKE GLOBAL CHANGES
for string in changes["all"].keys():
    if changes["all"][string].startswith("REGEX:"):
        filecontent = re.sub(string,r""+changes["all"][string][6:],filecontent)
    else:
        filecontent = re.sub(string,changes["all"][string],filecontent)

#CHECK IF FILE HAS CHANGES
if filename in changes["files"]:
    #PERFORM ALL FILE SPECIFIC CHANGES
    for string in changes["files"][filename].keys():
        if changes["files"][filename][string].startswith("REGEX:"):
            filecontent = re.sub(string,r""+changes["files"][filename][string][6:],filecontent)
        else:
            filecontent = re.sub(string,changes["files"][filename][string],filecontent)

#SAVE THE CHANGES
open(sys.argv[1],"w").write(filecontent)
