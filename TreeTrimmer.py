import sys
import json
import urllib.parse,subprocess

treeFile = sys.argv[1] #File with tree information
listOfFiles = "" #List to Dump back to the file
data = json.load(open(treeFile)) #

print(data)
for file in data:
    if file["type"] == "blob":
        listOfFiles+=urllib.parse.quote(file["path"])+" "
    if file["type"] == "tree":
        print(file["path"])
        subprocess.call("mkdir -p tmpfiles/"+file["path"],shell=True)


open(treeFile,"w+").write(listOfFiles) #Save into the file again
