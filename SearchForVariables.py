import os,re,sys

path = sys.argv[1]

filelist = []
varlist = []

for root,dirs,files in os.walk(path):
    for file in files:
        if "manifest.yaml" in file:
            filelist.append(os.path.join(root,file))

for file in filelist:
    string = open(file,"r").read()
    if "VARIABLE NAME" in string:
        varlist.append(file)

print("\nMissing variables:")
if varlist != []:
    for var in varlist:
        print(var)
else:
    print("No missing variable!")
